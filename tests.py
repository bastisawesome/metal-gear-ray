import unittest
import guild
import os
import json

class GuildTest(unittest.TestCase):
    def setUp(self):
        self.module_name = 'TestGuild'
        self.server_id = ''.join([str(ord(c)) for c in 'Test Server'])
    
    def tearDown(self):
        # Clean up persistent data
        try:
            os.remove(f'persistent/{self.server_id}.json')
        except Exception:
            pass
    
        self.module_name = None
        self.server_id = None

class TestGuildServerAccessor(GuildTest):
    def setUp(self):
        super().setUp()

        self.module_data = {
            'config_version': 0,
            'test_case_1': 'test',
            'test_case_2': 87
        }
        self.server_data = {
            self.module_name: self.module_data
        }
        self.server_accessor = guild._ServerAccessor(self.server_id, self.module_name)

        # Setup the persistent file
        with open(f'persistent/{self.server_id}.json', 'w') as f:
            json.dump(self.server_data, f)
    
    def tearDown(self):
        super().tearDown()

        # Clean up object information
        self.server_accessor = None
    
    def test_constructor(self):
        '''
        Tests that all values are set to the proper defaults
        '''
        self.assertEqual(self.server_accessor.server_id, self.server_id)
        self.assertEqual(self.server_accessor.module_name, self.module_name)
        self.assertEqual(self.server_accessor.file_path, 
            f'persistent/{self.server_id}.json')
        self.assertIsNone(self.server_accessor.server_data)
        self.assertIsNone(self.server_accessor.module_data)
        self.assertFalse(self.server_accessor.file_opened)
        self.assertIsNone(self.server_accessor.file)
    
    def test_load_existing_file(self):
        self.server_accessor.load()
        self.assertDictEqual(self.server_accessor.server_data, self.server_data)
        self.assertDictEqual(self.server_accessor.module_data, self.module_data)
    
    def test_load_no_file(self):
        os.remove(f'persistent/{self.server_id}.json')
        expected_server_data = {self.module_name: None}
        self.server_accessor.load()
        self.assertDictEqual(self.server_accessor.server_data,
            expected_server_data)
        self.assertIsNone(self.server_accessor.server_data[self.module_name])
        self.assertIsNone(self.server_accessor.module_data)
    
    def test_load_missing_module(self):
        with open(f'persistent/{self.server_id}.json', 'w') as f:
            json.dump({'different-test': {}}, f)
        with self.assertRaises(guild.MissingKeyError):
            self.server_accessor.load()
    
    def test_save(self):
        # Setup the file to be saved
        self.server_accessor.load()
        os.remove(f'persistent/{self.server_id}.json')
        self.server_accessor.save()

        # Pull test data
        data = None
        with open(f'persistent/{self.server_id}.json', 'r') as f:
            data = json.load(f)
        
        self.assertDictEqual(self.server_data, data)
        self.assertDictEqual(self.module_data, data[self.module_name])
    
    def test_get_module_data(self):
        self.server_accessor.load()
        self.assertDictEqual(self.module_data,
            self.server_accessor.get_module_data())
    
    def test_set_module_data(self):
        new_module_data = {'test_case_1': 'new test'}
        new_server_data = {self.module_name: new_module_data}
        self.server_accessor.load()
        self.server_accessor.set_module_data(new_module_data)
        self.assertDictEqual(self.server_accessor.module_data, new_module_data)
        self.assertDictEqual(self.server_accessor.server_data, new_server_data)
    
    def test_open_existing_file(self):
        self.server_accessor._open()
        self.assertTrue(self.server_accessor.file_opened)
        self.assertIsNotNone(self.server_accessor.file)
        self.assertEqual(self.server_accessor.file.mode, 'r+')
    
    def test_open_not_existing_file(self):
        os.remove(f'persistent/{self.server_id}.json')
        self.server_accessor._open()
        self.assertTrue(self.server_accessor.file_opened)
        self.assertIsNotNone(self.server_accessor.file)
        self.assertEqual(self.server_accessor.file.mode, 'w+')
    
    def test_open_twice(self):
        self.server_accessor._open()

        with self.assertRaises(guild.FileOpenedError):
            self.server_accessor._open()
    
    def test_close(self):
        self.assertFalse(self.server_accessor.file_opened)
        self.assertIsNone(self.server_accessor.file)

class TestMigrateRegister(GuildTest):
    def setUp(self):
        super().setUp()

        self.persistent_data = json.loads('{"config_version": 1, "role_menus": {"735907081533784064": {"\u2705": "Verified"}}}')
        self.persistent_data['server_id'] = self.server_id
    
    def tearDown(self):
        super().tearDown()

        # Clean up object information
        self.persistent_data = None

    def test_migrate_persistent_file_new_server(self):
        '''
        Test migrating a module from an older standard on a server that has not
        been migrated for.
        '''
        # Run the migration code
        # We are creating a copy of persistent_data instead of passing by
        # reference to ensure a proper comparison later on.
        guild.migrate_persistent_file(self.server_id, self.module_name, dict(self.persistent_data))

        # Clean up changes so the persistent_data more closely resembles the
        # expected dictionary.
        del self.persistent_data['server_id']

        # Hook into the server accessor to test migrations
        server_accessor = guild._ServerAccessor(self.server_id, self.module_name)
        server_accessor.load()

        # Test to make sure the migration was successful
        # First we need to make sure the data exists
        self.assertIsNotNone(server_accessor.server_data)
        self.assertIsNotNone(server_accessor.module_data)

        # Next, make sure all the data has been properly migrated
        module_data = server_accessor.module_data
        
        # Test each key:value pair to be equal and exist
        for key, value in self.persistent_data.items():
            self.assertIn(key, module_data)
            self.assertEqual(value, module_data[key])
    
    def test_migrate_persistent_file_existing_server(self):
        '''
        This is similar to the previous test, but this one ensures the server
        has already been migrated.
        '''
        # First we need to create the existing server
        server_accessor = guild._ServerAccessor(self.server_id, self.module_name)
        server_accessor.load()
        server_accessor.save()
        del server_accessor

        # At this point, it's the same as the previous test
        # Run the migration code
        # We are creating a copy of persistent_data instead of passing by
        # reference to ensure a proper comparison later on.
        guild.migrate_persistent_file(self.server_id, self.module_name, dict(self.persistent_data))

        # Clean up changes so the persistent_data more closely resembles the
        # expected dictionary.
        del self.persistent_data['server_id']

        # Hook into the server accessor to test migrations
        server_accessor = guild._ServerAccessor(self.server_id, self.module_name)
        server_accessor.load()

        # Test to make sure the migration was successful
        # First we need to make sure the data exists
        self.assertIsNotNone(server_accessor.server_data)
        self.assertIsNotNone(server_accessor.module_data)

        # Next, make sure all the data has been properly migrated
        module_data = server_accessor.module_data
        
        # Test each key:value pair to be equal and exist
        for key, value in self.persistent_data.items():
            self.assertIn(key, module_data)
            self.assertEqual(value, module_data[key])

    def test_migrate_persistent_file_migrated_module(self):
        '''
        This tests migrating an already migrated module. There's an expected
        exception thrown, and that's what we're testing for.
        '''
        guild.migrate_persistent_file(self.server_id, self.module_name, dict(self.persistent_data))

        with self.assertRaises(guild.CogMigratedError) as cm:
            guild.migrate_persistent_file(self.server_id, self.module_name, self.persistent_data)
        
        e = cm.exception
        self.assertEqual(e.module_name, self.module_name)
    
    def test_migrate_persistent_file_incorrect_server_id(self):
        '''
        Test proper exception handling when the server ID isn't properly
        formatted.
        '''
        # Prepare data
        server_id = int(self.server_id)

        with self.assertRaises(guild.DataTypeError) as cm:
            guild.migrate_persistent_file(server_id, self.module_name, self.persistent_data)

        e = cm.exception
        self.assertEqual(e.received_datatype, int)
        self.assertEqual(e.expected_datatype, str)
    
    def test_migrate_persistent_file_incorrect_module_name(self):
        '''
        Test proper exception handling when the module name isn't properly
        formatted.
        '''
        # Prepare data
        module_name = 0

        with self.assertRaises(guild.DataTypeError) as cm:
            guild.migrate_persistent_file(self.server_id, module_name, self.persistent_data)

        e = cm.exception
        self.assertEqual(e.received_datatype, int)
        self.assertEqual(e.expected_datatype, str)
    
    def test_migrate_persistent_file_incorrect_data_type(self):
        '''
        Test proper exception handling when the data isn't a dictionary.
        '''
        # Prepare data
        persistent_data = ['1', 'test', {'what': 'What?'}]

        with self.assertRaises(guild.DataTypeError) as cm:
            guild.migrate_persistent_file(self.server_id, self.module_name, persistent_data)

        e = cm.exception
        self.assertEqual(e.received_datatype, list)
        self.assertEqual(e.expected_datatype, dict)

    
    def test_migrate_persistent_file_missing_server_id(self):
        '''
        Test proper exception handling when the data is missing the server_id
        key.
        '''
        # Prepare data
        del self.persistent_data['server_id']

        with self.assertRaises(guild.MissingKeyError) as cm:
            guild.migrate_persistent_file(self.server_id, self.module_name, self.persistent_data)

        e = cm.exception
        self.assertEqual(e.key, 'server_id')
    
    def test_migrate_persistent_file_mismatched_server_id(self):
        '''
        Test proper exception handling when the server_ids are mismatched.
        '''
        # Prepare data
        self.persistent_data['server_id'] = self.server_id + '1'

        with self.assertRaises(guild.MismatchedIdError) as cm:
            guild.migrate_persistent_file(self.server_id, self.module_name, self.persistent_data)
            
        e = cm.exception
        # Test proper arguments passed to the exception
        self.assertEqual(e.id_1, self.server_id)
        self.assertEqual(e.id_2, self.persistent_data['server_id'])

    def test_register_module_new_server(self):
        '''
        Test registering a module on a server that hasn't been registered.
        '''
        # Start by registering the module
        # No setup is required for this, as it is a fresh server instance.
        sa = guild._ServerAccessor(self.server_id, self.module_name)
        guild.register_module(self.server_id, self.module_name, sa)

        # Test that the server has been registered
        self.assertIsNotNone(sa.server_data)
        self.assertIsNotNone(sa.module_data)

        # Test that the module name exists as a key
        self.assertIn(self.module_name, sa.server_data)

        # Test that the module data is empty
        self.assertEqual(sa.server_data[self.module_name], {})
        self.assertEqual(sa.module_data, {})
    
    def test_register_module_existing_server(self):
        '''
        Test registering a module on a server that has been registered.
        '''
        # Setup server data. Since the server needs to exist first, register
        # the server
        sa = guild._ServerAccessor(self.server_id, self.module_name)
        sa.load()

        # Register the module
        guild.register_module(self.server_id, self.module_name, sa)

        # Unlike the previous test, this one assumes the server data is not
        # None and only checks that the module data is not None.
        self.assertIsNotNone(sa.module_data)

        # The rest is the exact same as before, test that the module name is a
        # key in the server data.
        self.assertIn(self.module_name, sa.server_data)

        # Test that the module data is empty
        self.assertEqual(sa.server_data[self.module_name], {})
        self.assertEqual(sa.module_data, {})

    def test_register_module_migrated_cog(self):
        '''
        Test proper exception handling on a module that has already been
        registered.
        '''
        # First register the module, assuming everything works properly.
        guild.register_module(self.server_id, self.module_name)

        # Now that the register module function raises a CogRegisteredError
        with self.assertRaises(guild.CogRegisteredError) as cm:
            guild.register_module(self.server_id, self.module_name)

        # Make sure the function passes the correct data to the error class.
        e = cm.exception
        self.assertEqual(self.module_name, e.module_name)

class TestGuildHelpers(GuildTest):
    def setUp(self):
        super().setUp()

    def tearDown(self):
        super().tearDown()
    
    def prepare_server_persistent_data(self):
        self.module_data = {
            'config': 1,
            'test': 'test',
            'second_entry': 2
        }

        self.server_data = {
            self.module_name: self.module_data
        }

        self.serverAccessor = guild._ServerAccessor(self.server_id, self.module_name)
        self.serverAccessor.load()

    def cleanup_server_persistent_data(self):
        del self.module_data
        del self.server_data
        del self.serverAccessor

    def test_get_cog_data(self):
        self.prepare_server_persistent_data()
        
        self.serverAccessor.set_module_data(self.module_data)
        self.serverAccessor.save()

        # Test!
        data = guild.get_cog_data(self.server_id, self.module_name)
        self.assertDictEqual(data, self.module_data)

        self.cleanup_server_persistent_data()
    
    def test_set_cog_data(self):
        self.prepare_server_persistent_data()

        # Test!
        guild.set_cog_data(self.server_id, self.module_name, self.module_data)
        self.serverAccessor.load()

        self.assertDictEqual(self.module_data, self.serverAccessor.module_data)
        self.assertDictEqual(self.serverAccessor.server_data[self.module_name], self.module_data)

        self.cleanup_server_persistent_data()

    def test_is_module_registered(self):
        new_module_name = 'test_module_2'

        # Test when a server hasn't been registered
        is_registered = guild.is_module_registered(self.server_id, self.module_name)
        self.assertFalse(is_registered)

        # Test after the server has been registered
        guild.register_module(self.server_id, self.module_name)
        is_registered = guild.is_module_registered(self.server_id, self.module_name)
        self.assertTrue(is_registered)

        # Test when a new module hasn't been registered but the server has.
        is_registered = guild.is_module_registered(self.server_id, new_module_name)
        self.assertFalse(is_registered)

        # Test when a new module has been registered after the server has.
        guild.register_module(self.server_id, new_module_name)
        is_registered = guild.is_module_registered(self.server_id, new_module_name)
        self.assertTrue(is_registered)

class TestGuildDecorators(unittest.TestCase):
    def setUp(self):
        pass
    
    def tearDown(self):
        pass