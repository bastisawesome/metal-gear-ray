metal-gear-ray
==============

.. toctree::
   :maxdepth: 4

   bot
   errors
   extensions
   guild
   mgr_logger
   tests
