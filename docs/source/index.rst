.. metal-gear-ray documentation master file, created by
   sphinx-quickstart on Fri Sep 18 20:16:53 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to metal-gear-ray's documentation!
==========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
