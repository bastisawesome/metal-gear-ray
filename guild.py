'''Manages custom decorators and helper functions for working with persistent data.

This module manages servers' persistent files, either in JSON form
or in a database. This includes helper functions to access data, write data,
or migrate data from one format to another. When an access call is made, a new
server object is created to manage server persistent data, then destroyed
when access is finished. This server object is not shared outside this
module. Extensions can access server information by helper functions designed
to get or set the requested data. These functions manage accessing the server
object.

This module defines decorators for commonly used command checks, like
role checking. These decorators use the internal server object or helper
functions to handle their checks and are not self-contained. However, this
module is NOT in charge of ensuring proper access for commands to be executed,
that logic must be implemented at the cog level.

TODO:
    * Return full dictionary for modules instead of trying to parse it through manually. What the fuck was I thinking? This entire system is fucked up and should've been better planned. Why am I so incompetent sometimes? Anyway, that should resolve most issues I've been having, so please update to just let modules receive/send their own changes and sync in bulk instead of trying to manage through here. For now this is the best solution, there's probably some other method to do this, like through reflection, but fuck that.
'''

import json
import sys
import os
import pathlib
import discord
from discord.ext import commands
from errors import MGRError
from logging import CRITICAL, ERROR, WARNING, DEBUG, INFO
from bot import logger

'''
******************
****Exceptions****
******************
'''
class GuildError(MGRError):
    pass

class DataTypeError(GuildError):
    def __init__(self, received_datatype, expected_datatype):
        self.received_datatype = received_datatype
        self.expected_datatype = expected_datatype
        self.message = f'''Expected datatype `{self.expected_datatype}` but
received datatype `{self.received_datatype}`.'''
        super(DataTypeError, self).__init__(self.message)

class MissingKeyError(GuildError):
    def __init__(self, key):
        self.key = key
        self.message = f'Required key not found: {self.key}.'
        super().__init__(self.message)

class MismatchedIdError(GuildError):
    def __init__(self, id_1, id_2):
        self.id_1 = id_1
        self.id_2 = id_2
        self.message = f'ID {self.id_1} does not equal {self.id_2}'
        super().__init__(self.message)

class CogMigrationError(GuildError):
    pass

class CogMigratedError(CogMigrationError):
    def __init__(self, module_name):
        self.module_name = module_name
        self.message = f'{self.module_name} has already been migrated!'
        super().__init__(self.message)

class CogRegisteredError(GuildError):
    def __init__(self, module_name):
        self.module_name = module_name
        self.message = f'{self.module_name} has already been registered!'
        super().__init__(self.message)

class FileOpenedError(GuildError):
    def __init__(self, file_name):
        self.file_name = file_name
        self.message = f'''File '{self.file_name}' is already open!'''
        super().__init__(self.message)

'''
******************
****DECORATORS****
******************
'''
def has_role(role_name: str):
    def predicate(ctx: commands.Context):
        role = discord.utils.find(lambda r: r.name == role_name,
            ctx.guild.roles)
        return role in ctx.author.roles
    return commands.check(predicate)

'''
*************************
****GUILD PERSISTENCE****
*************************
'''
class _ServerAccessor:
    '''Manages connections to server-specific persistent data.

    Using the server id, opens the server-specific persistent JSON data file,
    saves the data in an internal server data cache, and then loads the
    extension-specific data into the internal extension data cache. This class
    DOES NOT handle accessing/modifying specific data and instead works with
    bulk data. Helper functions should handle more fine-tuned access and
    modifications, then sync them in mass as defined by functions below.

    Args:
        server_id: The server's ID, in string form, to open the
            persistent file.
        module_name: The extension's name, used to specify where to read/write
            data from/to. This does not impact the data being loaded into the
            server accessor.


    '''
    def __init__(self, server_id: str, module_name: str) -> None:
        self.server_id = server_id
        self.module_name = module_name
        self.file_path = 'persistent/{}.json'.format(self.server_id)
        '''
        Server data format:
        server_id.json ->
        {
            'module_name': {
                [module data]
            },
            'module_name': {
                [module data]
            },...
        }
        '''
        self.server_data = None
        self.module_data = None
        self.file_opened = False
        self.file = None
    
    def load(self):
        '''Reads data from the server persistent file, loads it into the server
        data variable, then closes the connection to the file.

        Raises:
            MissingKeyError: If an extension has not been migrated but a server
                has persistent data.
        
        '''
        try:
            self._open()
        except FileOpenedError as e:
            logger.error(e)
        
        server_data_exists = True
        try:
            self.server_data = json.load(self.file)
        except json.JSONDecodeError:
            # Assume the file is empty
            self.server_data = {}
            server_data_exists = False
        
        # Close file, all necessary data has been read
        self._close()

        # Attempt to fill in module data, create 
        data = None
        try:
            data = self.server_data[self.module_name]
        except KeyError:
            if server_data_exists:
                raise MissingKeyError(self.module_name)
            else:
                self.server_data[self.module_name] = None
        
        # Write module data to the internal module data cache.
        # THIS DOES NOT SYNC WITH THE FILE
        self.module_data = data
    
    def save(self):
        '''Writes the data to the server persistent file.'''
        try:
            self._open()
        except FileOpenedError as e:
            logger.error(e)
        
        json.dump(self.server_data, self.file)

        self._close()

    def get_module_data(self) -> dict:
        '''
        Returns the extension's data set by the server data, as it is currently
        synced. This function DOES NOT sync the extension's data with the file.

        Returns:
            The extension's full persistent data dictionary.
        
        '''
        return self.module_data
    
    def set_module_data(self, data: dict):
        '''
        Assigns the extension's data to the server data and internal module data
        cache. This function DOES NOT sync the extension's data with the file and
        should be combined with the save function after setting the extension's data.

        Args:
            data: Dictionary containing an extension's persistent data.
        
        '''
        self.module_data = data
        self.server_data[self.module_name] = self.module_data
    
    def _open(self):
        '''
        Creates a connection to a server's persistent file. This does not read the
        data, only opens the connection and signals the connection is open.

        Raises:
            FileOpenedError: If a file has already been opened by this accessor
                and not closed.
        '''
        if self.file_opened:
            raise FileOpenedError(self.file_path)
        
        # Check if file exists
        if pathlib.Path(self.file_path).exists():
            # Read the data
            self.file = open(self.file_path, 'r+')
        else:
            # Create new data
            self.file = open(self.file_path, 'w+')

        self.file_opened = True
        
    def _close(self):
        '''
        Closes the file connection. This function DOES NOT write the persistent
        data to the file.
        '''
        if self.file_opened:
            self.file.close()
            self.file_opened = False
    
    def __del__(self):
        '''Ensure the file connection is closed.'''
        if self.file_opened:
            self._close()

def migrate_persistent_file(server_id: str, module: str, data: dict) -> None:
    '''Migrates an extension's proprietary persistent file into a standardised
    persistent file.

    This function migrates all existing data from one persistent file into
    a single server-specific persistent file. Data should be properly
    formatted by the calling extension to ensure compatibility with the new standard.
    
    This function does a bare-bones check for the data format to ensure 
    proper compatibility, and raises an error on improperly formatted data, but
    this should not be mistaken for proper data handling.

    This function is much stricter than previous persistent handlers, and
    does complete checks on all data-types to ensure proper formatting.

    Extensions cannot call this function more than once. If an extension attempts to
    migrate data multiple times this function will raise an exception. Extensions
    are responsible for cleaning up their own persistent data.

    Args:
        server_id: ID of the server whose persistent file is being migrated.
        module: The name of the module being migrated. This is used to nest
            the passed data for organisation.
        data: Dictionary of data, properly formatted, that is to be
            migrated into the single-file server persistent JSON file.
            
            The formatting should be done as shown below:
                * The server ID should be copied into the data dictionary, should be a 
                    string, and should match the passed server_id argument.
                * The remaining data should be in the intended formatting, as that data
                    cannot and will not be checked here. The data will be directly copied under
                    the passed module name, as-is, with no formatting added.
                


    Raises:
        DataTypeError: If the incorrect types for ``server_id``, ``module``, or
            ``data`` are not the proper types.
        MissingKeyError: If the ``data`` dictionary does not contain a
            ``server_id`` key.
        MismatchedIdError: If the ``server_id`` key in the ``data`` dictionary
            does not match the ``server_id`` passed.
        CogMigratedError: If the calling extension has already been migrated.

    '''

    # Data checking
    # Ensure server_id is of type `str`
    if type(server_id) != str:
        raise DataTypeError(type(server_id), str)
    
    # Ensure module is of type `str`
    if type(module) != str:
        raise DataTypeError(type(module), str)

    # Ensure data is of type `dict`
    if type(data) != dict:
        raise DataTypeError(type(data), dict)

    # Ensure the data dictionary contains a server_id
    if not 'server_id' in data:
        raise MissingKeyError('server_id')

    # Ensure the server_id and the data's server_id match
    if not server_id == data['server_id']:
        raise MismatchedIdError(server_id, data['server_id'])
    
    # Create a new server object and create the persistent file from the data
    # dictionary
    server = _ServerAccessor(server_id, module)
    
    # Register the module before doing anything else
    try:
        register_module(server_id, module, server)
    except CogRegisteredError:
        raise CogMigratedError(module)

    # Module has been tested for common errors, migrate the data into the new
    # persistent file.
    # Clear the server_id from the data dictionary
    del data['server_id']
    server.set_module_data(data)

    # Write the data and close the file connection
    server.save()

def register_module(server_id: str, module_name: str, server_accessor=None) -> None:
    '''Registers an extension in the server's persistent data file.

    Registers an extension with the server's persistent data, by `module_name`,
    creating an empty dictionary object in the server's data cache. An extension
    cannot register itself to the server data and assign data at the same time,
    so multiple function calls are required. Furthermore, extensions should
    only register themselves once, so this function should be used in conjunction
    with `is_module_registered`.

    Args:
        server_id: Server ID to register the module in.
        module_name: The name of the module to be registered.
    
    Raises:
        CogRegisteredError: If an extension is already registered in a server's
            persistent data file.
    
    '''
    serverAccessor = server_accessor or _ServerAccessor(server_id, module_name)

    try:
        serverAccessor.load()
    except MissingKeyError:
        # This is expected behaviour: there's no registered module, so register
        # the module here. If an exception isn't raised, there's a problem.
        # WHAT THE FUCK IS THIS LOGIC?!

        # Looking back at it, the only time a MissingKeyError is raised is when
        # the server has a persistent file assigned to it. Because of this, an
        # extra check is needed. Nothing goes in here at all, this part just
        # ensures there's no crash due to weird, possibly faulty, behaviour.
        pass
    else:
        # Because if there's no exception being raised, someone done goofed.
        # Again, WHAT THE FUCK?!

        # Having double checked the server accessor logic, there is a chance
        # no except will be raised, so now there's an extra check to see if the
        # module data is null. If it's null, the module hasn't been registered
        # yes. So we can refuse to raise an exception here.

        if not serverAccessor.module_data is None:
            raise CogRegisteredError(module_name)
    
    # Moving previous logic here because of aforementioned logical failures.
    # Those logical errors are on my part, not on the program itself.
    
    # Previously, I assigned Nonetype to the module information.
    # However, logically it would make more sense to apply an empty dictionary,
    # as this would allow a failure on the module's part to error out. Empty
    # dictionaries are not None, so if a module fails to assign the data to the
    # persistent data, it still can't be registered again. It's not registered.
    serverAccessor.server_data[module_name] = {}
    serverAccessor.module_data = {}
    serverAccessor.save()

'''
**************************
*****HELPER FUNCTIONS*****
**************************
'''
# About fucking time...
def get_cog_data(server_id: str, module_name: str) -> dict:
    '''Returns the existing persistent data for an extension.

    This function does not verify that an extension has been registered or
    migrated, so extensions should ensure they are properly initialised beforehand
    to prevent errors.

    Args:
        server_id: The ID of the server calling the function.
        module_name: The name of the calling extension.
    
    Returns:
        The full extension's persistent data, as is stored in the file.
    
    '''
    serverAccessor = _ServerAccessor(server_id, module_name)
    serverAccessor.load()

    return serverAccessor.get_module_data()

def set_cog_data(server_id: str, module_name: str, data: dict):
    '''Writes the extension's persistent data to the server's data file.
    
    Overwrites the existing persistent data for an extension. Careless use of
    this function will overwrite data, so extensions should take care to call
    this only after getting the extension's data.

    This function does not verify that an extension has been registered or
    migrated, so the calling extension should make sure it has been properly 
    initialised to prevent errors.

    Args:
        server_id: The ID of the server calling the function.
        module_name: The name of the calling extension.
    
    '''
    serverAccessor = _ServerAccessor(server_id, module_name)
    serverAccessor.load()

    serverAccessor.set_module_data(data)
    serverAccessor.save()

def is_module_registered(server_id: str, module_name: str) -> bool:
    '''Determines if a module has already been migrated or not.'''
    serverAccessor = _ServerAccessor(server_id, module_name)
    try:
        serverAccessor.load()
    except MissingKeyError:
        return False
    
    return serverAccessor.module_data != None
