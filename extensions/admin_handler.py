import discord
from discord.ext import commands
import pathlib, json 

class AdminHandler(commands.Cog):
    def __init__(self, bot):
        self.bot: commands.Bot = bot
        self.CURRENT_CONFIG_VERSION = 1
        self.file_path = 'persistent/{}-adminhandler.json'
        '''
        server_id (str) : {
            config_version: (int),
            log_channel: (str)
        }
        '''
        self.server_data = {}
    
    def _create_server_file(self, server_id: str):
        '''
        Handles creation of the server persistent file.
        '''
        # Create server structure
        self.server_data[server_id] = {
            'config_version': self.CURRENT_CONFIG_VERSION,
            'log_channel': None
        }
        
        # Create server file from path
        self._write_server_file(server_id)
    
    def _read_server_file(self, server_id: str):
        '''
        Reads the corresponding server file into the server_data dictionary.
        '''
        data: dict = None
        with open(self.file_path.format(server_id), 'r') as f:
            data = json.loads(f.read())

        # Update server configuration if the config version is outdated.
        if data['config_version'] < self.CURRENT_CONFIG_VERSION:
            self._update_config(server_id, data)
        else:
            self.server_data[server_id] = data
    
    def _write_server_file(self, server_id: str):
        '''
        Writes the corresponding server data from the server_data dictionary
        into the server file.
        '''
        with open(self.file_path.format(server_id), 'w') as f:
            f.write(json.dumps(self.server_data[server_id]))

    def _update_config(self, server_id: str):
        '''
        Converts outdated configuration files into the latest version,
        ensuring compatibility between both versions. This is non-reversible
        '''
        old_version = data['version']
        pass

    async def pre_invoke_hook(self, server_id: str, ctx:commands.Context=None):
        '''
        Check to make sure the server file has been created and assigned
        to this object.
        '''
        # Check if server file exists
        path = pathlib.Path(self.file_path.format(server_id))
        if not path.exists():
            self._create_server_file(server_id)
        else:
            # Read the file into the current server data
            self._read_server_file(server_id)

    async def post_invoke_hook(self, server_id: str, ctx:commands.Context=None):
        '''
        Update the persistent server file to ensure data is properly recorded.
        '''
        self._write_server_file(server_id)

    '''
    Commands here
    '''
    @commands.command(hidden=True)
    async def set_log_channel(self, ctx: commands.Context, channel_id: str):
        '''
        Specify the channel in which to write logs.
        Only one channel can be specified for writing logs, and running the
        command again will override the previous log channel.
        '''
        server_id = str(ctx.guild.id)
        self.server_data[server_id]['log_channel'] = channel_id

        await ctx.send("Log channel set!")
    
    @commands.command(hidden=True)
    async def unset_log_channel(self, ctx: commands.Context):
        '''
        Removes the log channel from the server, effectively ending all server
        logging.
        '''
        server_id = str(ctx.guild.id)
        self.server_data[server_id]['log_channel'] = None

        await ctx.send("Log channel unset!")
    
    '''
    Listeners here
    '''
    @commands.Cog.listener()
    async def on_raw_message_delete(self, payload):
        '''
        Listener to track and log ALL messages deleted, not just cached
        messages.
        On any messages deleted, check if the server currently has a log channel
        assigned and write all known information to the log channel.
        Also ignore any changes made to the log channel itself.
        '''
        # Setup
        channel_id = str(payload.channel_id)
        server_id = str(payload.guild_id)
        message_id = str(payload.message_id)
        cached_message: discord.Message = payload.cached_message

        # Ensure the server has a configuration assigned
        await self.pre_invoke_hook(server_id)

        # Check if the server has a log channel, or if the event was triggered
        # in the log channel.
        if not self.server_data[server_id]['log_channel'] or \
            channel_id == self.server_data[server_id]['log_channel']:
            # Do nothing
            return
        
        server = self.bot.get_guild(int(server_id))
        channel = server.get_channel(int(channel_id))
        log_channel = server.get_channel(int(self.server_data[server_id]['log_channel']))

        # Prepare format string for outputting to the log channel
        '''
        Examples:
        Without cached message:
        Non-cached message deleted:
        Message ID: [message_id]
        Channel: [channel_name]

        With cached message:
        Message deleted:
        Text: [message data]
        Author: [message author/nick]
        Author tag: [message author tag]
        Author ID: [message author's ID]
        Message ID: [message_id]
        Channel: [channel_name]
        '''
        log_msg = ""

        # Generate log message from uncached message.
        if not cached_message:
            log_msg = f"Non-cached message deleted:\nMessage ID: {message_id}"
            log_msg += f"\nChannel: {channel.name}"
        else:
            author = cached_message.author
            log_msg = f"Message deleted:\nText: {cached_message.content}"
            log_msg += f"\nAuthor: {author.display_name}"
            log_msg += f"\nAuthor tag: {author.name}#{author.discriminator}"
            log_msg += f"\nAuthor ID: {author.id}"
            log_msg += f"\nMessage ID: {cached_message.id}"
            log_msg += f"\nChannel: {channel.name}"
        
        await log_channel.send(log_msg)
    
    @commands.Cog.listener()
    async def on_message_edit(self, before: discord.Message, 
        after: discord.Message):
        '''
        If a message has been cached, this event will be triggered when that
        message has been edited. Log edits to the server log channel, if any.
        Ignore if the edited message is in the log channel.
        '''
        # Setup
        channel = before.channel
        channel_id = str(channel.id)
        channel_name = channel.name
        server = before.guild
        server_id = str(server.id)
        author = before.author
        author_id = author.id
        author_name = author.display_name
        author_tag = author.name + '#' + author.discriminator

        # Ensure the server has a configuration assigned
        await self.pre_invoke_hook(server_id)

        # Check if server has a log channel, or if the event was triggered from
        # the log channel.
        if not self.server_data[server_id]['log_channel'] or \
            channel_id == self.server_data[server_id]['log_channel']:
            # Do nothing.
            return
        
        log_channel = server.get_channel(int(self.server_data[server_id]['log_channel']))

        '''
        Example:
        [message author name] has edited a message:
        Original text: [original message text]
        New text: [new message text]
        Channel: [channel name]
        Author tag: [author tag]
        Author ID: [author ID]
        Message ID: [message ID]
        '''
        log_msg = f"{author_name} has edited a message:"
        log_msg += f"\nOriginal text: {before.content}"
        log_msg += f"\nNew text: {after.content}"
        log_msg += f"\nChannel: {channel_name}"
        log_msg += f"\nAuthor tag: {author_tag}"
        log_msg += f"\nAuthor ID: {author_id}"
        log_msg += f"\nMessage ID: {before.id}"

        await log_channel.send(log_msg)

    async def cog_before_invoke(self, ctx):
        server_id = str(ctx.guild.id)
        await self.pre_invoke_hook(server_id, ctx)
    
    async def cog_after_invoke(self, ctx):
        server_id = str(ctx.guild.id)
        await self.post_invoke_hook(server_id, ctx)

def setup(bot):
    bot.add_cog(AdminHandler(bot))