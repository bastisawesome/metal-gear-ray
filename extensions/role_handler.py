import discord
from discord.ext import commands
import pathlib, json
import guild

'''
TODO:
- Fix writing to server data when a message cannot be found
- Fix bug: cannot assign role menu to messages from another channel
  - Add channel ID to parameters for searching for messages to add reaction roles
    to
'''

class RoleHandler(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.CURRENT_CONFIG_VERSION = 1
        self.file_path: str = "persistent/{}-rolehandler.json"
        '''
        {
            "server_id": id -> int,
            "config_version": num -> int
            "role_menus": {
                    "message_id": id -> int: "reactions": {"emote_id": "role_id"},
                },...
            ]
        }
        '''
        self.server_data: dict = {}
    
    async def _create_server_file(self, server_id: str):
        '''
        Handles creation of the server persistent file.
        '''
        # Create server structure
        self.server_data[server_id] = {
            'config_version': self.CURRENT_CONFIG_VERSION,
            'role_menus': {}
        }
        # Create server file from path
        await self._write_server_file(server_id)


    async def _read_server_file(self, server_id: str):
        '''
        Reads the corresponding server file into the server_data dictionary.
        '''
        data: dict = None
        with open(self.file_path.format(server_id), 'r') as f:
            data = json.loads(f.read())
        
        # Update server configuration if the config version is outdated.
        if data['config_version'] < self.CURRENT_CONFIG_VERSION:
            self._update_config(server_id, data)
        else:
            self.server_data[server_id] = data

    async def _write_server_file(self, server_id: str):
        '''
        Writes the corresponding server data from the server_data dictionary
        into the server file.
        '''
        with open(self.file_path.format(server_id), 'w') as f:
            f.write(json.dumps(self.server_data[server_id]))

    async def _update_config(self, server_id: str, data: dict):
        '''
        Converts outdated configuration files into the latest version,
        ensuring compatibility between both versions. This is non-reversible.
        '''
        old_version = data['version']
        '''
        Example:
        if old_version < some_old_version:
            [changes here]
        if old_version < slightly_newer_version:
            [more changes here]
        '''
        pass

    async def pre_invoke_hook(self, server_id: str, ctx=None):
        '''
        Check to make sure the server file has been created and assigned
        to this object.
        '''
        # Check if server file exists
        path = pathlib.Path(self.file_path.format(server_id))
        if not path.exists():
            await self._create_server_file(server_id)
        else:
            # Read the file into the current server data
            await self._read_server_file(server_id)

    async def post_invoke_hook(self, server_id: str, ctx=None):
        '''
        Update the persistent server file to ensure data is properly recorded.
        '''
        await self._write_server_file(server_id)

    @commands.command()
    @commands.is_owner()
    async def add_reaction_role(self, ctx: commands.Context, message_id: str, 
        reaction_emote: str, role: str):
        '''
        This function is used to assign a reaction to a message that will then
        assign a role to the users who react.
        If a message has not had a reaction role added to it, it needs to
        create a new entry in the server reaction messages.
        If the message id is already registered, it will need to check if the
        emote has been registered. If so, it will be unable to finish. If not,
        it will assign a role to that emote by appended to the emote/role lists.
        '''
        server_id = str(ctx.guild.id)
        server_react_messages = self.server_data[server_id]['role_menus']
        
        # First poll for messages
        if not message_id in server_react_messages.keys():
            # Add the message to the dictionary
            server_react_messages[message_id] = {}
        
        message_reactions = server_react_messages[message_id]

        # Poll for existing reactions
        if not reaction_emote in message_reactions.keys():
            # Add the reaction and role to the message
            message_reactions[reaction_emote] = role
            
            # Also add reaction to the message
            msg: discord.Message = await ctx.fetch_message(int(message_id))
            
            await msg.add_reaction(reaction_emote)

            await ctx.send("Reaction role successfully added!")
    
    @commands.command(hidden=True)
    async def remove_reaction_role(self, ctx: commands.Context, message_id: str,
        reaction_emote: str):
        '''
        This function is used to remove a reaction from a message, provided both
        the reaction and message exist within the server data.
        If a message no longer contains any reaction roles, that message is
        removed from the database.
        '''
        # Pre-configuration
        server_id = str(ctx.guild.id)
        server_react_messages = self.server_data[server_id]['role_menus']

        # Check that the message has already been assigned
        if message_id in server_react_messages.keys():
            message_reactions = server_react_messages[message_id]

            # Check that the emote has already been assigned
            if reaction_emote in message_reactions.keys():
                # Remove reaction
                del message_reactions[reaction_emote]

                # Check if the message has any reactions
                if len(message_reactions.keys()) == 0:
                    del server_react_messages[message_id]
                
                await ctx.send("Reaction role successfully removed!")

    @commands.command(hidden=True)
    async def check_reacts(self, ctx: commands.Context):
        server_id = str(ctx.guild.id)
        await ctx.send(str(self.server_data[server_id]))
    
    @commands.Cog.listener()
    async def on_raw_reaction_add(self, payload: discord.RawReactionActionEvent):
        '''
        Check if a message being reacted on is a reaction message. If there is
        a reaction role assigned to the message, see if that reaction has a role
        assigned. If there is a role assigned, assign that role to the user who
        reacted. If any of the above checks are false, do nothing.
        '''
        user = payload.member
        message_id = str(payload.message_id)
        server_id = str(payload.guild_id)
        emote = payload.emoji

        # Ignore if a bot user triggered this event
        if user.bot:
            return
        
        # Force a read of the server's persistent file
        await self.pre_invoke_hook(server_id)

        # Check for reaction menu
        menus = self.server_data[server_id]['role_menus']
        # Check if message is assigned
        if message_id in menus.keys():
            # Check if reaction is assigned
            if emote.name in menus[message_id].keys():
                role_name = menus[message_id][emote.name]
                role = discord.utils.get(user.guild.roles, name=role_name)
                await user.add_roles(role)
    
    async def cog_before_invoke(self, ctx):
        server_id = str(ctx.guild.id)
        await self.pre_invoke_hook(server_id, ctx)
    
    async def cog_after_invoke(self, ctx):
        server_id = str(ctx.guild.id)
        await self.post_invoke_hook(server_id, ctx)

def setup(bot):
    bot.add_cog(RoleHandler(bot))