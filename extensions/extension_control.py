import discord
from discord.ext import commands

class ExtensionControl(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        
    @commands.command(hidden = True)
    async def load(self, ctx, module=''):
        """Loads a module"""
        try:
            self.bot.load_extension('extensions.'+module)
        except Exception as e:
            await ctx.send("\N{PISTOL}")
            await ctx.send('{}: {}'.format(type(e).__name__, e))
        else:
            await ctx.send('\N{OK HAND SIGN}')
    
    @commands.command(hidden = True)
    async def unload(self, ctx, module=''):
        """Unloads a module"""
        try:
            self.bot.unload_extension('extensions.'+module)
        except Exception as e:
            await ctx.send("\N{PISTOL}")
            await ctx.send('{}: {}'.format(type(e).__name__, e))
        else:
            await ctx.send('\N{OK HAND SIGN}')
    
    @commands.command(name='reload', hidden=True)
    async def _reload(self, ctx, module=''):
        """Reloads a module."""
        try:
            self.bot.unload_extension('extensions.'+module)
            self.bot.load_extension('extensions.'+module)
        except Exception as e:
            await ctx.send('\N{PISTOL}')
            await ctx.send('{}: {}'.format(type(e).__name__, e))
        else:
            await ctx.send('\N{OK HAND SIGN}')
    
def setup(bot):
    bot.add_cog(ExtensionControl(bot))
