'''Description of the bot module goes here.

Todo:
    * Implement role permissions to commands
    * Implement ignored channels for admin_handler
    * Reorganise docs to fit the Google style-guide
'''
import discord
from discord.ext import commands

import logging
import mgr_logger

import os, sys
from dotenv import load_dotenv

# Globals

logger = mgr_logger.configure_logger()

basedir = os.path.abspath(os.path.dirname(__file__))

if __name__ == '__main__':
    load_dotenv(os.path.join(basedir, '.env'))

    TOKEN = os.getenv("TOKEN")

    bot = commands.Bot(command_prefix='!', description='Bot description!')

    def get_extensions() -> list:
        ext = list()
        for module in os.listdir('extensions'):
            if module[-3:] != '.py': continue
            if module == '__init__.py': continue
            
            ext.append(module[:-3])
        return ext

    @bot.event
    async def on_ready():
        logger.info('Logged in as:')
        logger.info(bot.user.name)
        logger.info(bot.user.id)
        logger.info('------')
        
        logger.info('Loading extensions...')
        
        failedExtensions = 0
        numExtensions = 0
        for extension in get_extensions():
            numExtensions += 1
            #try:
            bot.load_extension('extensions.'+extension)
            #except Exception as e:
                #exc = '{}: {}'.format(type(e).__name__, e)
                #logger.info('Failed to load extension {}\n{}'.format(extension, exc))
                #failedExtensions += 1
        
        logger.info('Extensions loaded. ({}/{} succeeded)'.format(numExtensions-failedExtensions, numExtensions))
        logger.info('------')

    bot.run(TOKEN, bot=True, reconnect=True)